# SimplonDelivery - TypeScript : Programmation fonctionnelle 

## Contexte du projet

SimplonDelivery, une entreprise spécialisée en logistique internationale, nous confie le développement d'un prototype d'application. Cette application, destinée à la gestion des conversions de devises et au calcul des frais de livraison, sera un outil clé dans l'optimisation de leurs services. 

Nous devons créer un prototype sans utiliser de programmation orientée objet.

## Objectifs & Notions utilisées :

### Objectif :

Développer une application en ligne de commande en TypeScript qui permet à l'utilisateur :
- de convertir des montants entre l'euro (EUR), le dollar canadien (CAD) et le yen japonais (JPY), 
- de calculer les frais de livraison et les frais de douane des colis.

### Notions :

- Initialisation d'un projet TypeScript avec Node
- Transpilation (/compilation) d'un projet TypeScript en JavaScript
- Exécution du JavaScript pour tester le fonctionnement
- Compréhension des notions suivantes en TypeScript :
    - Les types
    - Les déclarations de fonction
    - Les tableaux

## Installation

### Initialiser NPM

Dans le terminal :

    npm init -y

### Structure du projet

Créer les dossiers et les fichiers suivants :

    ├── dist
    │   └── index.js
    ├── src
    │   └── index.ts

### Installation de Typescript

#### Ajout de la dépendance
Dans le terminal :

    npm i --save-dev typescript @types/node

#### Configuration
Création d'un fichier tsconfig.json qui contiendra :

    {
      "compilerOptions": {
        "module": "nodenext",
        "moduleResolution": "nodenext",
        "target": "es2022",
        "strict": true,
        "esModuleInterop": true,
        "forceConsistentCasingInFileNames": true,
        "outDir": "./dist"
    },

    "include": ["src/**/*"],
    "exclude": ["**/*.spec.ts"]
    }

#### Build
Ajout dans la section script du fichier package.json : 

    "scripts": {
    "build": "tsc",
    }

### Installation de TS-Node et Nodemon
Pour lancer notre application sans passer par la compilation et que le serveur se mette à jour automatiquement à chaque changement sur le fichier Typescript

#### Ajout de la dépendance
Dans le terminal :

    npm i --save-dev ts-node

#### Configuration
Création d'un fichier nodemon.json qui contiendra :

    {
    "watch": ["src"],
    "ext": ".ts,.js",
    "ignore": [],
    "exec": "npx ts-node ./src/index.ts"
    }

#### Build
Ajout dans la section script du fichier package.json : 

    "scripts": {
    "dev": "nodemon",
    }

### Installation ESLint
La librairie ESLint permet de trouver et corriger rapidement des problèmes de syntaxe ou non respect de conventions.

#### Ajout de la dépendance
Dans le terminal :

    npm i --save-dev eslint @typescript-eslint/eslint-plugin @typescript-eslint/parser

#### Configuration
Création d'un fichier .eslintrc qui contiendra :

    {
    "root": true,
    "parser": "@typescript-eslint/parser",
    "plugins": ["@typescript-eslint"],
    "extends": ["eslint:recommended", "plugin:@typescript-eslint/recommended"],
    "rules": {},
    "env": {
        "browser": true,
        "node": true,
        "es2022": true
    }
    }

### Installation Prettier
La librairie Prettier va nous permettre de formater le code selon des conventions définies et partagées dans le code.

#### Ajout de la dépendance
Dans le terminal :

    npm i --save-dev prettier eslint-config-prettier eslint-plugin-prettier

#### Configuration
Création d'un fichier .eslintrc qui contiendra :

    {
    "trailingComma": "all",
    "singleQuote": true,
    "printWidth": 80
    }

#### Mise à jour de la configuration Eslint 
Modification à faire dans .eslintrc :

    {
    "root": true,
    "parser": "@typescript-eslint/parser",
    "plugins": ["@typescript-eslint", "prettier"],
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "prettier"
    ],
    "rules": {
        "prettier/prettier": 2
    },
    "env": {
        "browser": true,
        "node": true,
        "es2022": true
    }
    }


### Installation de Jest pour les Tests unitaires
Jest est le framework de test qui nous simplifie la configuration et écriture des tests.

#### Ajout de la dépendance
Dans le terminal :

    npm i --save-dev jest @types/jest ts-jest eslint-plugin-jest 

#### Configuration
Création d'un fichier jest.config.ts qui contiendra :

    module.exports = {
        transform: {
            '^.+\\.ts?$': 'ts-jest',
        },
        testEnvironment: 'node',
        testRegex: './src/.*\\.(test|spec)?\\.(ts|ts)$',
        moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
        roots: ['<rootDir>/src'],
    };

#### Mise à jour de la configuration Eslint 
Modification à faire dans .eslintrc :

    {
    "root": true,
    "parser": "@typescript-eslint/parser",
    "plugins": ["@typescript-eslint", "prettier", "jest"],
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "prettier"
    ],
    "rules": {
        "prettier/prettier": 2
    },
    "env": {
        "browser": true,
        "node": true,
        "es2022": true,
        "jest/globals": true
    }
    }

### Création d'un .gitignore

Possibilité de créer un fichier .gitignore avec le site gitignore.io et de rajouter les éléments suivants :

    # IntelliJ project files
    .idea
    *.iml
    out
    gen

    # VSCode
    .vscode/*
    !.vscode/settings.json
    !.vscode/tasks.json
    !.vscode/launch.json
    !.vscode/extensions.json
    !.vscode/*.code-snippets

    # Local History for Visual Studio Code
    .history/

    # Built Visual Studio Code Extensions
    *.vsix

    # Folders
    dist
    coverage
    node_modules

## Commandes utiles

Pour transpiler le code du fichier Typescript index.ts vers le fichier Javascript index.js :

    npm run build

Pour lancer l'application à partir du fichier index.js :

    node dist

Pour lancer le serveur qui se mettra à jour à chaque changement sur notre fichier index.ts :

    npm run dev

Pour exécuter du TS sans passer par la compilation : 

    npx ts-node ./src/index.ts

## Execution de l'application

Transpiler le code avec la commande :

    npm run build

Lancer l'application avec la commande :

    node dist

Il vous sera demander de saisir vos données dans le terminal.