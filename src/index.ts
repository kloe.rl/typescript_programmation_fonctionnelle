// Import du module prompt
const prompt = require('prompt');

// Schema pour valider les donées saisies par l'utilisateur
const schema = {
    properties: {
        weight: {
            pattern: /^\b([1-9]|[1-4][0-9]|5[0-5])\b/,
            message: 'Weight are in kg, integers only. Value must be between 1 and 55',
            required: true
        },
        length: {
            pattern: /^\b([1-9]|[1-9][0-9]|1[0-4][0-9]|150)\b/,
            message: 'Dimensions are in cm, integers only. Value must be between 1 and 150',
            required: true
        },
        width: {
            pattern: /^\b([1-9]|[1-9][0-9]|1[0-4][0-9]|150)\b/,
            message: 'Dimensions are in cm, integers only. Value must be between 1 and 150',
            required: true
        },
        height: {
            pattern: /^\b([1-9]|[1-9][0-9]|1[0-4][0-9]|150)\b/,
            message: 'Dimensions are in cm, integers only. Value must be between 1 and 150',
            required: true
        },
        destination: {
            pattern: /France|Canada|Japan+$/,
            message: 'Destinations must be France, Canada or Japan',
            required: true
        },
        declaredValue: {
            pattern: /^\d/,
            message: 'Declared value must be an integer.',
            required: true
        }, 
        currency: {
            pattern: /EUR|CAD|JPY+$/,
            message: 'Currency are EUR for Euro, CAD for Canadian Dollar and JPY for Yen',
            required: true
        }
    }
};

// Démarrage de prompt
prompt.start();

// Récupération des valeurs via le terminal et execution des fonctions
prompt.get(schema, function (err: string, results: { weight: number, length: number, width: number, height: number, destination: string, declaredValue: number, currency: string }) {
    // Déclaration des variables pour forcer le type number
    const weight: number = results.weight;
    const length: number = results.weight;
    const width: number = results.weight;
    const height: number = results.weight;
    const declaredValue: number = results.declaredValue;

    // Fonction de calcul des coûts d'envoi
    console.log(shippingCalculator(
        weight,
        length,
        width,
        height,
        results.destination
    ))

    // Fonction de calcul des frais de douanes
    console.log(customsCalcutor(
        declaredValue,
        results.currency,
        results.destination
    ))

    // Fonction qui calcule le montant total
    console.log(totalCostCalculator(
        shippingCost,
        customsCost
    ))
});

// Déclaration des variables dont nous aurons besoin tous au long de l'application
let convertedAmount: number = 0;
let shippingCost: number = 0;
let currencyDestination: string = "";
let customsCost: number = 0;

// Fonction qui permet de convertir un montant donné dans une autre deveise parmi EUR CAD ou JPY
// amount est obligatoirement un nombre, currencyIn et currencyOut sont des chaines de caratères
// le résultat est une chaine de caractère comprenant le montant converti et le nom de la devise
function conversionCalculator(amount: number, currencyIn: string, currencyOut: string): number {

    if (currencyIn === "EUR" && currencyOut === "CAD") {
        convertedAmount = amount * 1.5
    } else if (currencyIn === "EUR" && currencyOut === "JPY") {
        convertedAmount = amount * 130
    } else if (currencyIn === "CAD" && currencyOut === "EUR") {
        convertedAmount = amount * 0.67
    } else if (currencyIn === "CAD" && currencyOut === "JPY") {
        convertedAmount = amount * 87
    } else if (currencyIn === "JPY" && currencyOut === "EUR") {
        convertedAmount = amount * 0.0077
    } else if (currencyIn === "JPY" && currencyOut === "CAD") {
        convertedAmount = amount * 0.0115
    }
    return convertedAmount;
};

// console.log(conversionCalculator(100, "JPY", "EUR"));

// Fonction qui permet d'établir le montant de la livraison en fonction du poids, des dimensions et de la destination
// weight, length, width et height sont des nombres, destination est une chaine de caratères.
// le résultat est une chaine de caractère comprenant le montant et le nom de la devise

function shippingCalculator(weight: number, length: number, width: number, height: number, destination: string): string {
    // variable qui contiendra le nom de le devise en fonction du pays de destination
    let currencyDestination: string = "";

    // Donne le nom de la devise en fonction du pays de destination
    if (destination === "France") {
        currencyDestination = "EUR"
    } else if (destination === "Canada") {
        currencyDestination = "CAD"
    } else if (destination === "Japan") {
        currencyDestination = "JPY"
    };

    // Calculateur des frais d'envoi
    if (weight <= 1) {
        if (destination === "France") {
            shippingCost = length + width + height > 150 ? 15 : 10;
        } else if (destination === "Canada") {
            shippingCost = length + width + height > 150 ? 22.5 : 15;
        } else if (destination === "Japan") {
            shippingCost = length + width + height > 150 ? 1500 : 1000;
        };
    } else if (weight > 1 && weight <= 3) {
        if (destination === "France") {
            shippingCost = length + width + height > 150 ? 25 : 20;
        } else if (destination === "Canada") {
            shippingCost = length + width + height > 150 ? 37.5 : 30;
        } else if (destination === "Japan") {
            shippingCost = length + width + height > 150 ? 2500 : 2000;
        }
    } else if (weight > 3) {
        if (destination === "France") {
            shippingCost = length + width + height > 150 ? 35 : 30;
        } else if (destination === "Canada") {
            shippingCost = length + width + height > 150 ? 52.5 : 45;
        } else if (destination === "Japan") {
            shippingCost = length + width + height > 150 ? 3500 : 3000;
        }
    }

    // Création d'une variable qui renvoit un message avec les couts d'envoi et la devise
    // Cela permet de conserver le cout d'envoi (shippingCost) typé number et de le manipuler plus tard
    const shippingCostMessage: string = `Shipping cost for this parcel is ${shippingCost} ${currencyDestination}`
    return shippingCostMessage;
};

// console.log(shippingCalculator(3, 10, 10, 10, "Japan"));
// console.log('shipping cost :', shippingCost)

// Fonction qui permet de calculer les frais de douane en focntion de la valeur déclarée du colis et le pays de destination
// declaredValue est un nombre et destination est une chaîne de caractère.
// le résultat est une chaine de caractère comprenant un message, le montant des frais de douanes et le nom de la devise
function customsCalcutor(declaredValue: number, currency: string, destination: string): string {

    // Conditions qui récupère le nom de la devise en fonction de la destination
    if (destination === "France") {
        currencyDestination = "EUR"
    } else if (destination === "Canada") {
        currencyDestination = "CAD"
    } else if (destination === "Japan") {
        currencyDestination = "JPY"
    };

    // Conversion de la valeur déclarée du colis dans la devise du pays de destination
    const convertedDeclaredValue = conversionCalculator(declaredValue, currency, currencyDestination)

    // Calculateur des frais de douane
    if (convertedDeclaredValue >= 20 && destination === "Canada") {
        customsCost = (declaredValue * 0.15)
    } else if (convertedDeclaredValue >= 5000 && destination === "Japan") {
        customsCost = (declaredValue * 0.1)
    } else if (destination === "France") {
        customsCost = 0
    };

    // Création d'une variable qui renvoie un message avec les frais de douane et la devise
    // Cela permet de conserver les frais de douane (customsCost) typé number et de le manipuler plus tard
    const customsCostMessage: string = `Customs fees are ${customsCost} ${currencyDestination}`
    return customsCostMessage;
};

// console.log('customs :', customsCalcutor("EUR", 300, "Japan"));

function totalCostCalculator (shippingCost:number, customsCost:number):string {
    let totalCost:number = shippingCost + customsCost
    const totalCostMessage:string = `Total cost for this parcel is ${totalCost} ${currencyDestination}`
    return totalCostMessage
}